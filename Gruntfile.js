'use strict';

module.exports = function (grunt) {

  grunt.initConfig({
    watch: {
      files: ["scss/*.scss", "twig/*.twig"],
      tasks: ['sass', 'postcss:dist', 'csscomb:dist', 'twigRender']
    },
    // "sass"-task configuration
    sass: {
      options: {
        sourceMap: true,
        outputStyle: 'expanded',
      },
      dist: {
        files: {
          'assets/css/main.css' : 'scss/main.scss'
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions','ie >= 10']
      },
      dist: { // Target
        files: {
          'assets/css/main.css': 'assets/css/main.css'
        }
      }
    },
    csscomb: {
      src: {
        expand: true,
        cwd: 'scss/',
        src: ['*.scss'],
        dest: 'scss/'
      },
      dist: {
        expand: true,
        cwd: 'assets/css/',
        src: ['*.css', '!*.min.css'],
        dest: 'assets/css/'
      },
    },

    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer'),
          require('postcss-flexbugs-fixes')
        ]
      },
      dist: {
        src: 'assets/css/main.css'
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : [
            'assets/css/*.css',
            'assets/js/*.js',
            './*.html'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: ["./"]
          }
        }
      }
    },

    twigRender: {
      dev: {
        files : [
          {
            data: './config.json',
            expand: true,
            cwd: './twig/',
            src: ['**/*.twig', '!**/_*.twig'], // Match twig templates but not partials
            dest: './',
            ext: '.html'   // index.twig + datafile.json => index.html
          }
        ]
      },
    },
  });

  // load all grunt tasks
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-csscomb');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-twig-render');

  // the default task (running "grunt" in console) is "watch"
  grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('twig', ['twigRender']);
};