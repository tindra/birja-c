jQuery(document).ready(function($){
  var $body = $('.body'),
      $nav = $('[data-nav]'),
      $navTrigger = $('[data-nav-trigger]'),
      openClass = 'open',
      activeClass = 'active',
      navActiveClass = 'nav-active';

  // Nav
  $navTrigger.click(function(e){
    e.preventDefault();

    var $this = $(this);

    if ($(window).width() < 1024) {
      if ($this.hasClass(activeClass)) {
        $this.removeClass(activeClass);
        $nav.removeClass(activeClass);
        $body.removeClass(navActiveClass);
      } else {
        $this.addClass(activeClass);
        $nav.addClass(activeClass);
        $body.addClass(navActiveClass);
      }
    }
  });

  /* Hero */
  var $heroToggle = $('[data-hero-toggle]'),
      $hero = $('[data-hero]'),
      heroToggleLS = localStorage.getItem('heroToggle');

  if (heroToggleLS != null) {
    if (heroToggleLS === 'collapsed') {
      $hero.addClass('collapsed');
    } else {
      $hero.removeClass('collapsed');
    }
  }

  $heroToggle.click(function(e) {
    e.preventDefault();

    if ($hero.hasClass('collapsed')) {
      $hero.removeClass('collapsed');
      localStorage.removeItem('heroToggle');
    } else {
      $hero.addClass('collapsed');
      localStorage.setItem('heroToggle', 'collapsed');
    }
  });

  /* Filter */
  var $filter = $("[data-filter]");
  if ($filter.length > 0) {
    var filterOffsetTop = $filter.offset().top,
        filterHeight = $filter.height();
    $(window).on('scroll load', function() {
      if ($(this).scrollTop() >= filterOffsetTop) {
        $filter.addClass('fixed');
        $filter.css('height', filterHeight);
      } else {
        $filter.removeClass('fixed');
        $filter.css('height', '');
      }
    });
  }

  /* Dropdown */
  var $dropdownToggle = $('[data-dropdown-toggle]'),
      $dropdownContent = $('[data-dropdown-content]'),
      dropdownActiveId = $dropdownToggle.attr('[data-active-id]');

  $dropdownToggle.click(function(e) {
    e.preventDefault();

    $(this).parent().siblings('.open').find($dropdownContent).hide();
    $(this).parent().siblings('.open').removeClass('open');

    $(this).next($dropdownContent).slideToggle(200);
    $(this).parent().toggleClass('open');
  });

  $dropdownContent.click(function(e) {
    e.preventDefault();

    if (e.target.classList.contains('dropdown__link')) {
      var newActiveId = e.target.getAttribute('data-id'),
          newValue = e.target.textContent,
          dropdownToggle = e.target.parentNode.parentNode.parentNode.querySelector('[data-dropdown-toggle]'),
          dropdownContent = e.target.parentNode.parentNode;

      if (newActiveId != dropdownActiveId) {
        e.target.parentNode.parentNode.querySelector('.active').classList.remove('active');
        e.target.classList.add('active');
        dropdownToggle.setAttribute('data-active-id', newActiveId);
        dropdownToggle.innerHTML = newValue;
        e.target.parentNode.parentNode.parentNode.parentNode.classList.remove('open');
        dropdownContent.style = '';
      }
    }
  });

  /* Info */
  var $infoToggle = $('[data-info-toggle]'),
      $infoContent = $('[data-info-content]');

  $infoToggle.click(function(e) {
    e.preventDefault();

    $(this).closest('.info').toggleClass('open');
  });

  /* Video toggle */
  $('[data-video]').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    gallery: {
      enabled: true
    },
    fixedContentPos: false
  });

  $('[data-video_gallery]').magnificPopup({
    type: 'iframe',
    delegate: '.video',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    gallery: {
      enabled: true
    },
    fixedContentPos: false
  });

  /* Masked Input */
  $('.js-tel').mask("+7(999)999-99-99");

  /* Custom control */
  $(".custom-control__input").prop("indeterminate", true);

  /* Smooth scroll */
  $('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
        && 
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          event.preventDefault();
          $('html, body').animate({
            scrollTop: target.offset().top - 115
          }, 1000, function() {
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
              return false;
            } else {
              $target.attr('tabindex','-1');
              $target.focus();
            };
          });
        }
      }
  });
});